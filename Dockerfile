WORKDIR /

COPY server ./
COPY templates/ ./templates
COPY static/ ./static
EXPOSE 8080
EXPOSE 80/tcp
EXPOSE 80/udp
EXPOSE 443/tcp
EXPOSE 8081/udp
ENTRYPOINT ["/server"]
