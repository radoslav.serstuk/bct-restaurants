BINARY_NAME=server

build: 
        GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ${BINARY_NAME} .  
run: 
        go build -o ${BINARY_NAME} 
        ./${BINARY_NAME}
clean:
        go clean
