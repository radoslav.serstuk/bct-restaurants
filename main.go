package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"

	//"log"
	"strings"
	//"strings"
	"regexp"

	"github.com/gin-gonic/gin"
	"github.com/gocolly/colly"
	"github.com/sashabaranov/go-openai"
	"golang.org/x/net/html"
)

var UrlRestaurants = map[string]string{
	"penzionset": "https://www.penzionset.sk/sk/restauracia",
	"rubikon":    "https://rubikon-restauracia.sk/",
	"pepes":      "http://www.pepes.sk/menu/",
	"pavilon":    "https://menucka.sk/denne-menu/kosice/spolocensky-pavilon",
}

// Info about daily menu
type FoodMenu struct {
	Restaurant string `json:"Restaurant"  yaml:"Restaurant"`
	Time       string `json:"Time"  yaml:"Time"`
	Date       string `json:"Day"  yaml:"Day"`
	Soups      []Food `json:"Soups"  yaml:"Soups"`
	MainFood   []Food `json:"MainFood"  yaml:"MainFood"`
}

// Info about main food
type Food struct {
	Name     string `json:"Name" yaml:"Name"`
	Weight   string `json:"Weight" yaml:"Weight"`
	Alergens string `json:"Alergens" yaml:"Alergens"`
	Price    string `json:"Price" yaml:"Price"`
}

var everyMenu = []FoodMenu{}

//MapOfRegexPatterns:= make(map[int][]string)

var patternsMap = map[string]string{
	"0": `^(\d+.*?\dg)?\s?(\D.*\D)\s*\((\d+.*\d|\d)\)\s*(\d.*\d €)?$`,
	"1": `^(?:\d\.)?(\d+,\d+[A-Z]?|\d*g)?(/)? (.*[a-z][.]?)?(?:/A )?(\d.*)/ (\d.*€)$`,
	"2": `^([^()]+)?([ \d+\. ]?)(?: \(alergény: )?([^()]+)?\)? (\d.* [g|l])$`,
	"3": `([\d,]*\d+\s*[a-zA-Z]+)\s*(.*?)*(\([\d+\,+]{1,}\))*\s*?$`,
}

func Scapper1(ch chan FoodMenu, wg *sync.WaitGroup) {
	// TODO allowed domains not working !!!
	wg.Add(1)
	c := colly.NewCollector(
	//colly.AllowedDomains(colly.Async(true)
	//colly.CacheDir("./cache1"), // cache prevents multiple download of site
	)

	c.OnRequest(func(r *colly.Request) {
		fmt.Printf("Visiting %s...\n", r.URL.String())
	})

	var menu *FoodMenu
	c.OnHTML("html", func(f *colly.HTMLElement) {
		temp0 := f.DOM
		menu = &FoodMenu{
			Time:       temp0.Find("span.timer").Text(),
			Date:       f.ChildText(".box1-inside > h2.text-uppercase > span"),
			Restaurant: temp0.Find("title").Text(),
		}
	})
	c.OnHTML(".box1-inside", func(e *colly.HTMLElement) {
		e.ForEach(".menu-polozka", func(_ int, el *colly.HTMLElement) {

			text := strings.TrimSpace(el.Text)
			text = strings.ReplaceAll(text, "	", "")
			fmt.Println(text)
			// ^(\d+.*?\dg)?\s?(\D.*\D)\s*\((\d+.*\d|\d)\)\s*(\d.*\d €)?$

			regex := regexp.MustCompile(patternsMap["0"])

			matches := regex.FindStringSubmatch(text)
			tmpSoupsorMeal := Food{}
			//fmt.Println("match1", matches[1], "match2", matches[2], "matches3", matches[3])
			if len(matches) <= 1 {
				log.Println("no matches in func1", "text", text)
			} else {
				if matches[1] != "" {
					tmpSoupsorMeal.Name = matches[2]
					tmpSoupsorMeal.Weight = matches[1]
					tmpSoupsorMeal.Alergens = matches[3]
					tmpSoupsorMeal.Price = matches[4]
					menu.MainFood = append(menu.MainFood, tmpSoupsorMeal)
				} else if matches[4] == "" {
					tmpSoupsorMeal.Name = matches[1]
					tmpSoupsorMeal.Alergens = matches[2]
					menu.Soups = append(menu.Soups, tmpSoupsorMeal)
				} else if matches[4] != "" {
					tmpSoupsorMeal.Name = matches[2]
					tmpSoupsorMeal.Weight = matches[1]
					tmpSoupsorMeal.Alergens = matches[3]
					tmpSoupsorMeal.Price = matches[4]
					menu.MainFood = append(menu.MainFood, tmpSoupsorMeal)
				} else {
					// TODO this will broke the program
					log.Println("no matches in func1", "text", text)

				}
			}

		})
	})
	c.Visit(UrlRestaurants["penzionset"])
	defer wg.Done()
	//everyMenu = append(everyMenu, menu)
	ch <- *menu
}

func Scapper2(ch chan FoodMenu, wg *sync.WaitGroup) {
	// TODO allowed domains not working !!!
	wg.Add(1)
	url := "https://rubikon-restauracia.sk/"
	response, err := http.Get(url)
	if err != nil {
		log.Fatalf("Error making GET request: %v", err)
	}
	defer response.Body.Close()

	// Parse the HTML response
	doc, err := html.Parse(response.Body)
	if err != nil {
		log.Fatalf("Error parsing HTML: %v", err)
	}

	// Find the first article element and extract its content
	menuText := findFirstArticleContent(doc)

	if menuText != "" {
		fmt.Println("Extracted Food Menu:")
	} else {
		fmt.Println("Article not found.")
	}

	token := os.Getenv("gptapi")
	c := openai.NewClient(token)
	ctx := context.Background()
	question := `Parse the prompt and return only food menu from it without counters like 1. , if it has weight like 0,33L then its soup, in Slovak and json format like

{
        "Restaurant": "Example Restaurant",
        "Time": "Lunch",
        "Day": "Monday",
        "Soups": [
            {
                "Name": "Karfiólová polievka so zeleným hráškom",
                "Weight": "0,33L",
                "Alergens": "A 1,7",
                "Price": "1,20€"
            }
        ],
        "MainFood": [
            {
                "Name": "Bravčové na šampiňónoch, tarhoňa – príloha",
                "Weight": "150g",
                "Alergens": "A 1,3",
                "Price": "5,00€"
            },

        ]
    }`

	req := openai.CompletionRequest{
		Model:     openai.GPT3Dot5TurboInstruct,
		MaxTokens: 1000,
		Prompt:    question + menuText,
	}

	resp, err := c.CreateCompletion(ctx, req)
	if err != nil {
		fmt.Printf("Completion error: %v\n", err)
		return
	}
	//fmt.Println("RESPONSE \n", resp.Choices[0].Text)
	var menu *FoodMenu
	menu = &FoodMenu{}
	menu = &FoodMenu{}

	if err := json.Unmarshal([]byte(resp.Choices[0].Text), &menu); err != nil {
		fmt.Println("Error:", err)
		return
	}

	// Marshal the Menu struct back to JSON
	jsonData, err := json.Marshal(menu)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	// Print the JSON data
	fmt.Println(string(jsonData))
	defer wg.Done()
	//everyMenu = append(everyMenu, *menu)
	ch <- *menu

}

/*
   Use goroutines to speed up the scraping process by allowing multiple pages to be loaded simultaneously.
   Instead of using regular expressions, use CSS selectors with the Colly library to extract information.
   Remove unnecessary code lines and comments
   Add the allowed domains that the user wants to scrape
   Refactor the code and make it more readable by breaking it down into smaller functions and providing meaningful variable names.
   Use struct tags to define the JSON format of struct fields

*/

func Scapper3(ch chan FoodMenu, wg *sync.WaitGroup) {
	// TODO allowed domains not working !!!
	wg.Add(1)
	c := colly.NewCollector(
	//colly.AllowedDomains("http://www.pepes.sk/menu/"),
	//	colly.Async(true),
	//colly.CacheDir("./cache3"), // cache prevents multiple download of site
	)

	c.OnRequest(func(r *colly.Request) {
		fmt.Printf("Visiting %s...\n", r.URL.String())
	})

	var menu *FoodMenu
	menu = &FoodMenu{}

	c.OnHTML("head", func(f *colly.HTMLElement) {
		menu = &FoodMenu{
			Restaurant: f.ChildAttr("meta[name=\"DESCRIPTION\"]", "content"),
		}
	})

	c.OnHTML("table > tbody > tr > td", func(f *colly.HTMLElement) {
		menu.Date = f.ChildText("font")
	})
	soupOrMeal := -1
	c.OnHTML("div.menu-item", func(e *colly.HTMLElement) {

		e.ForEach("div[class=\"row\"]", func(_ int, e *colly.HTMLElement) {
			//text := e.ChildText("div[class=\"inner\"]")
			text := e.ChildText("div[class=\"row\"]")
			//	fmt.Println("RAW", e.Index)
			if e.Index == 0 {
				soupOrMeal += 1
			}
			text = strings.ReplaceAll(text, "\n", " ")
			text = strings.Trim(text, " ")
			s := strings.ReplaceAll(text, "  ", "")
			s = strings.ReplaceAll(s, "Polievka:", "")

			s = strings.ReplaceAll(s, "Hlavné jedlo:", "")
			s = strings.ReplaceAll(s, "	", "")
			s = strings.Trim(s, " ")

			//str := "Polievka:Kláštorná polievka, chlieb (alergény: múka, vajce)0,33 l"
			// ^([^()]+)?([ \d+\. ]?)(?: \(alergény: )?([^()]+)?\)? (\d.* [g|l])$
			regex := regexp.MustCompile(patternsMap["2"])
			matches := regex.FindStringSubmatch(s)
			if len(matches) > 0 {

				tmpSoupsorMeal := Food{
					Name:     matches[1], // "Polievka:Kláštorná polievka, chlieb"
					Weight:   matches[4], // "0,33 l"
					Alergens: matches[3], // "múka, vajce"
					//Price:    arr[2],

				}

				if soupOrMeal > 0 {
					if tmpSoupsorMeal.Name != "" {
						menu.MainFood = append(menu.MainFood, tmpSoupsorMeal)
					} else if tmpSoupsorMeal.Weight != "" { //&& tmpSoupsorMeal.Price != ""  && tmpSoupsorMeal.Alergens != ""
						iterNum := len(menu.MainFood) - 1
						if tmpSoupsorMeal.Name != "" {
							menu.MainFood[iterNum].Name = menu.MainFood[iterNum].Name + " + " + tmpSoupsorMeal.Name
						}
						menu.MainFood[iterNum].Alergens = menu.MainFood[iterNum].Alergens + " " + tmpSoupsorMeal.Alergens
						menu.MainFood[iterNum].Weight = menu.MainFood[iterNum].Weight + " + " + tmpSoupsorMeal.Weight
					}

				} else if tmpSoupsorMeal.Name != "" {
					menu.Soups = append(menu.Soups, tmpSoupsorMeal)
				}
			}

		})
	})

	c.Visit(UrlRestaurants["pepes"])
	defer wg.Done()
	//everyMenu = append(everyMenu, *menu)
	ch <- *menu
}

func Scapper4(ch chan FoodMenu, wg *sync.WaitGroup) {
	// TODO allowed domains not working !!!
	wg.Add(1)
	defer wg.Done()
	c := colly.NewCollector(
	//colly.AllowedDomains(colly.Async(true)
	//colly.CacheDir("./cache1"), // cache prevents multiple download of site
	)

	c.OnRequest(func(r *colly.Request) {
		fmt.Printf("Visiting %s...\n", r.URL.String())
	})
	var menu *FoodMenu
	c.OnHTML("html", func(f *colly.HTMLElement) {
		temp0 := f.DOM
		menu = &FoodMenu{
			Time:       temp0.Find("span.timer").Text(),
			Date:       f.ChildText(".box1-inside > h2.text-uppercase > span"),
			Restaurant: f.ChildText("div[class=\"name\"]"),
		}

	})
	duplicateFilter := make(map[string]bool, 15)
	c.OnHTML("div[class=\"row\"]", func(e *colly.HTMLElement) {
		e.ForEachWithBreak("div[class=\"col-xs-10 col-sm-10\"]", func(_ int, el *colly.HTMLElement) bool {
			text := strings.TrimSpace(el.Text)
			fmt.Println(text)
			//text = "400 g       Ragú z tofu (120g) so zeleninou a bambusovými výhonkami, trojfarebná ryža, polníček (6,12,7)"
			regex := regexp.MustCompile(`([\d,]*\d+\s*[a-zA-Z]+)\s*(.*?)*(\([\d+\,+]{1,}\))*\s*?$`)
			matches := regex.FindStringSubmatch(text)
			if text == "" {
				return false
			}
			if len(matches) > 0 && duplicateFilter[matches[2]] != true {
				tmpSoupsorMeal := &Food{
					Name:     matches[2], //"Ragú z tofu (120g) so zeleninou a bambusovými výhonkami, trojfarebná ryža, polníček"
					Weight:   matches[1], //"400 g"
					Alergens: matches[3], // "(6,12.7)"
					//Price:    arr[2],
				}
				duplicateFilter[tmpSoupsorMeal.Name] = true
				if strings.Contains(tmpSoupsorMeal.Weight, "l") {
					menu.Soups = append(menu.Soups, *tmpSoupsorMeal)
				} else {
					menu.MainFood = append(menu.MainFood, *tmpSoupsorMeal)
				}
				return true
			} else if len(matches) == 0 {
				return true
			}

			return false

		})

	})
	c.Visit(UrlRestaurants["pavilon"])
	ch <- *menu
}

func Scrapper3_test() {
	c := colly.NewCollector(
		//	colly.AllowedDomains("pepes.sk"),
		colly.CacheDir("./cache5"), // cache prevents multiple download of site
	)
	//soupFlag := 0
	// Extract the food menu
	c.OnHTML("div.menu-item", func(e *colly.HTMLElement) {
		fmt.Println("Today's menu:")
		e.ForEach("div.inner", func(_ int, cat *colly.HTMLElement) {
			text := cat.ChildText("div[class=\"row\"]")
			//fmt.Println("CC", cat.Name, cat.Index, cat.Text)
			text = strings.ReplaceAll(text, "\n", " ")
			text = strings.Trim(text, " ")
			s := strings.ReplaceAll(text, "  ", "")
			fmt.Println("SS", s)
		})
		/*e.ForEach(".text, .alergeny, strong, .header, .add", func(_ int, cat *colly.HTMLElement) {
			//	fmt.Println("catName", cat.DOM, cat.Index, cat.Name, cat.Request.Headers, cat.Text)
			fmt.Println("catName", cat.Name, cat.Index)
			switch cat.Name {
			case "div":
				if cat.Text == "Polievka" {
					soupFlag = 1
				} else if cat.Text == "Hlavné jedlo (5,90 €)" {
					soupFlag = 0
				}
				fmt.Printf("Div: %s\n", cat.Text)
			case "strong":
				fmt.Printf("Strong: %s\n", cat.Text)
			}
			fmt.Println(soupFlag)
			switch cat.Index {
			case 1:
				{
					fmt.Println(nil)
				}
			}

		})*/
	})

	// Visit the website
	err := c.Visit("https://pepes.sk")
	if err != nil {
		log.Fatal(err)
	}

}

var funcNum = 4
var ch = make(chan FoodMenu, funcNum)
var menus = []FoodMenu{}

func main() {

	var wg sync.WaitGroup

	go Scapper1(ch, &wg) // penzionset
	go Scapper2(ch, &wg) // rubikon
	go Scapper3(ch, &wg) // pepes
	go Scapper4(ch, &wg) // pavilon
	//Scrapper3_test()

	wg.Wait()
	if ch != nil {

		for i := 0; i < funcNum; i++ {
			//fmt.Printf("Result: %v \n", <-ch)
			menus = append(menus, <-ch)
		}
	}
	fmt.Println("menus", menus)

	r := gin.Default()
	r.LoadHTMLGlob("templates/*")
	r.Static("/static", "./static")

	r.GET("/", renderHtml)

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}

func renderHtml(c *gin.Context) {
	if menus != nil {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"menus": menus,
		})
	} else {
		c.JSON(http.StatusBadRequest, "badrequest")
		return
	}
}

// Find and extract the content of the first article element
func findFirstArticleContent(n *html.Node) string {
	var inArticle bool
	var articleText string

	var traverse func(*html.Node)
	traverse = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "article" {
			inArticle = true
		}

		if inArticle && n.Type == html.TextNode {
			articleText += strings.TrimSpace(n.Data) + "\n"
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			traverse(c)
		}

		if n.Type == html.ElementNode && n.Data == "article" {
			inArticle = false
		}
	}

	traverse(n)
	return articleText
}
