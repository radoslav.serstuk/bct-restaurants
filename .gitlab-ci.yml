image: golang:latest
  
default:
  tags:
    - custom

stages:
  - prepare
  - test
  - security
  - build
  - code_quality
  - pages
  - security_scan
  - deepcode
  - secret_detection
  - sast

services:
  - docker:23.0-dind
#  - docker:dind  cannot be used because of this https://gitlab.com/gitlab-org/gitlab-runner/-/issues/37325 when trying to run with custom runners
  - docker:20.10.12-dind


include:
  - template: Secret-Detection.gitlab-ci.yml 
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
#  - template: Jobs/License-Scanning.gitlab-ci.yml
  - template: Jobs/Code-Intelligence.gitlab-ci.yml  

#include:
 # - template: Jobs/Code-Quality.gitlab-ci.yml
  #- template: Jobs/Dependency-Scanning.gitlab-ci.yml
 # - template: Jobs/License-Scanning.gitlab-ci.yml
#include:
#    - project: 'renovate-bot/renovate-runner'
#      file: '/templates/renovate.gitlab-ci.yml'

variables:
#  GO_PROJECT_PATH: /go/src/github.com/radoslav.serstuk/bct-restaurants
#  DOCKER_IMAGE: golang:latest
#  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2 #version2 for fix ERROR: Job failed (system failure): prepare environment: Error response from daemon: Cannot link to a non running container
  FULL_IMAGE_NAME: myapp:latest
  GEMNASIUM_CACHE_KEY: "scanners-cache"
  TRIVY_CACHE_KEY: "trivy-cache"

cache:
  #key: $GEMNASIUM_CACHE_KEY
  #paths:
  #  - .gemnasium-db/

  key: $TRIVY_CACHE_KEY
  paths:
    - .cache/trivy/


before_script:
#  - mkdir -p $GO_PROJECT_PATH
#  - ln -svf $CI_PROJECT_DIR $GO_PROJECT_PATH
  #- cd $GO_PROJECT_PATH
  #- apt-get update -qy && apt-get install -y docker.io
  #- docker info
  - echo $PATH

fmt:
  stage: test
  script:
    - unformatted_files=$(gofmt -l .)
    - if [ ! -z "$unformatted_files" ]; then
        echo "The following files are not formatted correctly:";
        echo "$unformatted_files";
        exit 1;
      fi
  allow_failure: true
  #only:
  #  - main
  when: manual

lint:
  stage: test
  script:
    - cd $GO_PROJECT_PATH
    - curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin latest
    - cd $CI_PROJECT_DIR
    - golangci-lint run
  allow_failure: true
  only:
    - main
  when: manual

test:
  stage: test
  script:
    - cd $CI_PROJECT_DIR
   # - docker pull $DOCKER_IMAGE
   # - docker build --cache-from=$DOCKER_IMAGE:latest -t $DOCKER_IMAGE:latest .
    - go test -v
  allow_failure: true
  only:
    - main
  when: manual
build_app:
  stage: build
  allow_failure: true
  script:
    - cd $CI_PROJECT_DIR
   # - docker pull $DOCKER_IMAGE
    - go build -o myapp
  artifacts:
    expire_in: 1 day
    paths:
      - myapp
  only:
    - main
  when: manual
code_quality_report:
  stage: prepare
  allow_failure: true
  script:
    - cd /
    - apt-get install git -y 
    - git clone https://github.com/gojp/goreportcard.git
    - cd goreportcard
    - make install
    - go install ./cmd/goreportcard-cli
    - cd $CI_PROJECT_DIR
    - echo $(go env GOPATH)
    - which goreportcard-cli
    - /go/bin/goreportcard-cli -v 
 # only:
  #  - main
  except:
   - schedule  
  when: manual
pages_markdown:
  stage: pages
  allow_failure: true
  script:
    - cd $CI_PROJECT_DIR
    - apt-get update -qy
    - apt-get install -y python3-pip
    - apt-get install -y python3.11-venv
    - python3 -m venv venv
    - source venv/bin/activate
    - pip3 install markdown2
    - mkdir public/
    - markdown2 README.md > public/index.html
    - ls
    - pwd 
  artifacts:
    expire_in: 1 day
    paths:
      - public/
  only:
    - main
  when: manual
trivy_security_scan:
  stage: security
  allow_failure: true
  image:
    name: aquasec/trivy:0.45.0-amd64
    entrypoint: [""]
# before_script:   
#    - mkdir -p $GO_PROJECT_PATH
#    - ln -svf $CI_PROJECT_DIR $GO_PROJECT_PATH
  script:
    - cd $CI_PROJECT_DIR
    - trivy --version
    - trivy fs . --cache-dir .cache/trivy/
    #- trivy image $FULL_IMAGE_NAME
  except:
    - schedule
  cache:
    key: $TRIVY_CACHE_KEY
    paths:
      - .cache/trivy/
    when: on_success
  when: manual
#deepCode:
#  stage: prepare
#  image: python:3-buster
#  allow_failure: true
#  script:
#    - cd $GO_PROJECT_PATH
#    - pip install -q deepcode
#    - deepcode -a $API_KEY analyze --path $GO_PROJECT_PATH --with-linters -txt
#  only:
#    - main

renovate:
  stage: prepare
  image: renovate/renovate:latest
  allow_failure: true
  before_script: []
  script:
    - |
      renovate --platform gitlab --endpoint $CI_API_V4_URL --token $RENOVATE_TOKEN
  only:
    - schedule
  variables:
    RENOVATE_TOKEN: $RENOVATE_TOKEN
  when: manual
go_test_coverage:
  stage: test
  allow_failure: true
  script:
    - cd $CI_PROJECT_DIR
    - go test -coverprofile=coverage.out ./...
    - go tool cover -func=coverage.out
    - go tool cover -html=coverage.out -o coverage.html
  artifacts:
    paths:
      - coverage.out
      - coverage.html
  when: manual

# https://docs.gitlab.com/ee/user/application_security/sast/
sast:
  stage: security
  before_script: []
  variables:
    CI_DEBUG_TRACE: "false"
  artifacts:
    paths:
      - gl-sast-report.json
    expire_in: 1 week 
  when: manual
# https://docs.gitlab.com/ee/user/application_security/secret_detection/
secret_detection:
  stage: security
  before_script: []
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"
    SECRETS_DEFAULT_BRANCH: 'cicd-testing'
  artifacts:
    paths:
      - gl-secret-detection-report.json
    expire_in: 1 week 
  when: manual
dependency_scanning:
  stage: security

code_quality:
  allow_failure: true
  stage: test

code_intelligence_go:
  allow_failure: true
  stage: prepare

#ci_pages:
#  image: ruby:2.6
#  before_script:
#    - gem install bundler
#    - bundle install
#  stage: pages
#  variables:
#    JEKYLL_ENV: production
#    LC_ALL: C.UTF-8
#  only:
#    - main
#  script:
#   # - bundle exec jekyll build -d test
#    - bundle exec jekyll build -d public
#  artifacts:
#    paths:
#      - public

#TODO : implement https://docs.gitlab.com/ee/ci/environments/incremental_rollouts.html 
#- incremental rollout 10%
#- incremental rollout 25%
#- incremental rollout 50%
#- incremental rollout 100%
#https://about.gitlab.com/blog/2016/11/03/publish-code-coverage-report-with-gitlab-pages/
