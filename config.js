module.exports = {
  
  platform: 'gitlab',
  endpoint: process.env.CI_API_V4_URL,
  token: process.env.RENOVATE_TOKEN,
  logLevel: 'info', 
  printConfig: true,
  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ['config:base'
        ],
    },
  repositories: ['radoslav.serstuk/bct-restaurants'
    ],
  platform: 'gitlab',
  username: 'renovate-bot',
};
