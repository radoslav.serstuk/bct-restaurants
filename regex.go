package main

import (
	"fmt"
	"regexp"
	"strings"
)

func rgx() {

	str := "400 g       Ragú z tofu (120g) so zeleninou a bambusovými výhonkami, trojfarebná ryža, polníček (6,12,7)"

	regex := regexp.MustCompile(`(\d+\s*[a-zA-Z]+)\s+(.*?)(\([\d+\,+]{1,}\))\s*?$`)
	// `\([\d+\,+]{1,}\)`
	matches := regex.FindStringSubmatch(str)

	if len(matches) > 0 {
		weight := matches[1] // "400 g"
		notes := matches[2]  // "Ragú z tofu (120g) so zeleninou a bambusovými výhonkami, trojfarebná ryža, polníček"
		extras := matches[3] // "(6,12)"

		fmt.Println(weight)
		fmt.Println(notes)
		fmt.Println(strings.TrimSpace(extras))

	}

}
